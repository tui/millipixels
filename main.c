#include "main.h"

#include <cairo/cairo.h>
#include <errno.h>
#include <fcntl.h>
#include <linux/videodev2.h>
#include <linux/media.h>
#include <linux/v4l2-subdev.h>
#include <math.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <time.h>
#include <assert.h>
#include <limits.h>
#include <linux/kdev_t.h>
#include <sys/sysmacros.h>
#include <asm/errno.h>
#include <wordexp.h>
#include <gtk/gtk.h>
#include <locale.h>
#include <zbar.h>
#include "camera_config.h"
#include "quickpreview.h"
#include "io_pipeline.h"
#include "wb.h"
#include "src/focus.h"

enum user_control { USER_CONTROL_ISO, USER_CONTROL_SHUTTER, USER_CONTROL_WHITE_BALANCE, USER_CONTROL_FOCUS };

static bool camera_is_initialized = false;
static bool camera_is_present = false;
static const struct mp_camera_config *camera = NULL;
static MPCameraMode mode;

static int preview_width = -1;
static int preview_height = -1;

static bool gain_is_manual = false;
static int gain;
static int gain_max;
static int gain_min;

static bool exposure_is_manual = false;
static int exposure;
static int exposure_max;
static int exposure_min;

static bool focus_is_manual = true;
static int focus;
static int focus_phase;

static bool wb_is_manual = true;

static const bool sw_auto = true;

static int movie_start = 0;

/*
 * TODO calc default from control min/max values, but for dw9714 0 is far,
 * 1023 is near; set the default in between.
 */
static int focus = 250;
static int wb = 3;

static bool has_auto_focus_continuous;
static bool has_auto_focus_start;

static cairo_surface_t *surface = NULL;
static cairo_surface_t *status_surface = NULL;
static char last_path[260] = "";

static MPZBarScanResult *zbar_result = NULL;

static int burst_length = 3;

static enum user_control current_control;

char *debug_message = "Debug message";

// Widgets
GtkWidget *preview;
GtkWidget *error_box;
GtkWidget *error_message;
GtkWidget *main_stack;
GtkWidget *open_last_stack;
GtkWidget *thumb_last;
GtkWidget *process_spinner;
GtkWidget *control_box;
GtkWidget *control_name;
GtkAdjustment *control_slider;
GtkWidget *control_auto;
static GtkWidget *camera_missing;
static GtkWidget *window;
static GtkWidget *movie;

static void draw_controls(void);

int
remap(int value, int input_min, int input_max, int output_min, int output_max)
{
	const long long factor = 1000000000;
	long long output_spread = output_max - output_min;
	long long input_spread = input_max - input_min;

	long long zero_value = value - input_min;
	zero_value *= factor;
	long long percentage = zero_value / input_spread;

	long long zero_output = percentage * output_spread / factor;

	long long result = output_min + zero_output;
	return (int)result;
}

static void
update_io_pipeline(void)
{
	struct mp_io_pipeline_state io_state = {
		.camera = camera,
		.burst_length = burst_length,
		.preview_width = preview_width,
		.preview_height = preview_height,
		.gain_is_manual = gain_is_manual || sw_auto,
		.gain = gain,
		.exposure_is_manual = exposure_is_manual || sw_auto,
		.exposure = exposure,
		.focus = focus,
		.wb = wb,
	};
	mp_io_pipeline_update_state(&io_state);
}

#define FIXUP(x, res) if (x < x##_min) { x = x##_min; res = 1; } if (x > x##_max) { x = x##_max; res = 1; }

static int auto_exposure(const struct image_stats *stats)
{
	int step, res = 0;
	step = exposure / 16;
	if (step < 1)
		step = 1;
	exposure += step * stats->exposure_adjust;
	FIXUP(exposure, res);
	return res;
}

static int auto_gain(const struct image_stats *stats)
{
	int step, res = 0;

	step = gain / 16;
	if (step < 1)
		step = 1;
	gain += step * stats->exposure_adjust;
	FIXUP(gain, res);
	return res;
}

static void auto_focus_start(void)
{
	focus_phase = 0;
	focus = 0;
}

#define PH_SWEEP 5
#define PH_DONE 6

static void auto_focus_step(const struct image_stats *stats)
{
	static uint64_t best_sharpness, best_focus;
	static const bool debug;

	if (focus_phase >= PH_DONE) {
		focus_phase++;
		if (stats->sharpness < best_sharpness * 0.6) {
			if (debug) printf("Focus lost, restart.\n");
			auto_focus_start();
		}
		return;
	}
	if (debug) printf("Phase %d, sharp %d best %d ", focus_phase, (int)stats->sharpness / 10000, (int)best_sharpness/ 10000);
	if (focus_phase < PH_SWEEP) {
		best_sharpness = 0;
		focus = 200;
		focus_phase ++;
		best_focus = 0;
		if (debug) printf("...prepare\n");
		goto set;
	}
	if (stats->sharpness > best_sharpness) {
		if (debug) printf("Still improving, focus %d\n", focus);
		best_focus = focus;
		best_sharpness = stats->sharpness;
		focus += 10;
		goto set;
	}
	if (stats->sharpness < best_sharpness * 0.8) {
		if (debug) printf("AF done?\n");
		focus = best_focus - 10;
		focus_phase = PH_DONE;
		goto set;
	}
	if (focus > 1023) {
		if (debug) printf("Finished range\n");
		focus = best_focus - 10;
		focus_phase = PH_DONE;
		goto set;
	}
	if (debug) printf("Not improving\n");
	focus += 10;
 set:
	set_focus(focus);
}

void auto_adjust(const struct image_stats *stats)
{
	if (sw_auto) {
		if (!exposure_is_manual && gain_is_manual)
			auto_exposure(stats);
		if (exposure_is_manual && !gain_is_manual)
			auto_gain(stats);
		if (!exposure_is_manual && !gain_is_manual) {
			int res;
			int gain_limit = 64;
			/* when exposure == exposure_min, we have too much light, use gain

			   when gain is < gain_limit, noise is acceptable, and we can adjust exposure -- FIXME!
			   when exposure == exposure_max, we have way too little light, 
			 */

			if ((stats->exposure_adjust < 0) == (gain > gain_limit))
				res = auto_gain(stats);
			else
				res = auto_exposure(stats);

			if (res)
				res = auto_exposure(stats);
			if (res)
				res = auto_gain(stats);
			if (res)
				printf("Out of control authority\n");
		}
	}

	if (!wb_is_manual) {
		int wb_min = 0;
		int wb_max = sizeof(WB_ILLUMINANTS) / sizeof(WB_ILLUMINANTS[0]) - 1;
		int res;
		wb += stats->balance_adjust;
		FIXUP(wb, res);
		if (stats->balance_adjust)
			mp_calculate_matrices(wb);
		if (res)
			printf("White balance beyond limits\n");
	}

	if (!focus_is_manual)
		auto_focus_step(stats);

	update_io_pipeline();
}

static bool
update_state(const struct mp_main_state *state)
{
	if (!camera_is_initialized) {
		camera_is_initialized = true;
		if (camera == state->camera) {
			gain = state->gain;
			gain_min = state->gain_min;
			gain_max = state->gain_max;
			exposure = state->exposure;
			exposure_min = state->exposure_min;
			exposure_max = state->exposure_max;
			has_auto_focus_continuous = state->has_auto_focus_continuous;
			has_auto_focus_start = state->has_auto_focus_start;
			if (camera->hasfocus) {
				set_focus(focus);
			}
		}
	}

	if (camera_is_present) {
		gtk_widget_hide(camera_missing);
	} else {
		gtk_widget_show(camera_missing);
	}

	if (camera == state->camera) {
		mode = state->mode;

		camera_is_present = state->is_present;
	}

	draw_controls();

	return false;
}

void
mp_main_update_state(const struct mp_main_state *state)
{
	struct mp_main_state *state_copy = malloc(sizeof(struct mp_main_state));
	*state_copy = *state;

	g_main_context_invoke_full(g_main_context_default(), G_PRIORITY_DEFAULT_IDLE,
				   (GSourceFunc)update_state, state_copy, free);
}

static bool set_zbar_result(MPZBarScanResult *result)
{
	if (zbar_result) {
		for (uint8_t i = 0; i < zbar_result->size; ++i) {
			free(zbar_result->codes[i].data);
		}

		free(zbar_result);
	}

	zbar_result = result;
	gtk_widget_queue_draw(preview);

	return false;
}

void mp_main_set_zbar_result(MPZBarScanResult *result)
{
	g_main_context_invoke_full(g_main_context_default(), G_PRIORITY_DEFAULT_IDLE,
				   (GSourceFunc)set_zbar_result, result, NULL);
}

static bool
set_preview(cairo_surface_t *image)
{
	if (surface) {
		cairo_surface_destroy(surface);
	}
	surface = image;
	gtk_widget_queue_draw(preview);
	return false;
}

void
mp_main_set_preview(cairo_surface_t *image)
{
	g_main_context_invoke_full(g_main_context_default(), G_PRIORITY_DEFAULT_IDLE,
				   (GSourceFunc)set_preview, image, NULL);
}

static void transform_centered(cairo_t *cr, uint32_t dst_width, uint32_t dst_height,
	                       int src_width, int src_height)
{
	cairo_translate(cr, dst_width / 2, dst_height / 2);

	double scale = MIN(dst_width / (double)src_width, dst_height / (double)src_height);
	cairo_scale(cr, scale, scale);

	cairo_translate(cr, -src_width / 2, -src_height / 2);
}

void
draw_surface_scaled_centered(cairo_t *cr, uint32_t dst_width, uint32_t dst_height,
			     cairo_surface_t *surface)
{
	cairo_save(cr);

	int width = cairo_image_surface_get_width(surface);
	int height = cairo_image_surface_get_height(surface);
	transform_centered(cr, dst_width, dst_height, width, height);

	cairo_set_source_surface(cr, surface, 0, 0);
	cairo_paint(cr);
	cairo_restore(cr);
}

struct capture_completed_args {
	cairo_surface_t *thumb;
	char *fname;
};

static bool
capture_completed(struct capture_completed_args *args)
{
	strncpy(last_path, args->fname, 259);

	gtk_image_set_from_surface(GTK_IMAGE(thumb_last), args->thumb);

	gtk_spinner_stop(GTK_SPINNER(process_spinner));
	gtk_stack_set_visible_child(GTK_STACK(open_last_stack), thumb_last);

	cairo_surface_destroy(args->thumb);
	g_free(args->fname);

	return false;
}

void
mp_main_capture_completed(cairo_surface_t *thumb, const char *fname)
{
	struct capture_completed_args *args = malloc(sizeof(struct capture_completed_args));
	args->thumb = thumb;
	args->fname = g_strdup(fname);
	g_main_context_invoke_full(g_main_context_default(), G_PRIORITY_DEFAULT_IDLE,
				   (GSourceFunc)capture_completed, args, free);
}

void
notify_processing_finished(cairo_surface_t *thumb) {
	mp_main_capture_completed(thumb, "");
}

const int gain_x = 16;
const int exposure_x = gain_x + 60;
const int balance_x = exposure_x + 60;
const int focus_x = balance_x + 60;
const int debug_x = focus_x + 60;

const int desc_y = 16;
const int value_y = 26;
const int max_y = 32;

static void
draw_controls(void)
{
	cairo_t *cr;
	char iso[21];
	char shutterangle[21];
	char f[12];
	char wb_text[12];

	if (exposure_is_manual) {
		sprintf(shutterangle, "%d", exposure);
	} else if (sw_auto) {
		sprintf(shutterangle, "a %d", exposure);
	} else {
		sprintf(shutterangle, "auto");
	}

	if (gain_is_manual) {
		sprintf(iso, "%d", gain);
	} else if (sw_auto) {
		sprintf(iso, "a %d", gain);
	} else {
		sprintf(iso, "auto");
	}

	if (focus_is_manual) {
		sprintf(f, "%d", focus);
	} else {
		sprintf(f, "a %d", focus);
	}

	if (wb_is_manual) {
		sprintf(wb_text, "%s", WB_ILLUMINANTS[wb]);
	} else {
		sprintf(wb_text, "a %s", WB_ILLUMINANTS[wb]);
	}

	if (status_surface)
		cairo_surface_destroy(status_surface);

	// Make a service to show status of controls, 32px high
	if (gtk_widget_get_window(preview) == NULL) {
		return;
	}
	status_surface =
		gdk_window_create_similar_surface(gtk_widget_get_window(preview),
						  CAIRO_CONTENT_COLOR_ALPHA,
						  preview_width, 32);

	cr = cairo_create(status_surface);
	cairo_set_source_rgba(cr, 0, 0, 0, 0.0);
	cairo_paint(cr);

	// Draw the outlines for the headings
	cairo_select_font_face(cr, "sans-serif", CAIRO_FONT_SLANT_NORMAL,
			       CAIRO_FONT_WEIGHT_BOLD);
	cairo_set_font_size(cr, 9);
	cairo_set_source_rgba(cr, 0, 0, 0, 1);

	cairo_move_to(cr, gain_x, desc_y);
	cairo_text_path(cr, "Gain");
	cairo_stroke(cr);

	cairo_move_to(cr, exposure_x, desc_y);
	cairo_text_path(cr, "Exposure");
	cairo_stroke(cr);

	cairo_move_to(cr, balance_x, desc_y);
	cairo_text_path(cr, "Balance");
	cairo_stroke(cr);

	if (camera->hasfocus) {
		cairo_move_to(cr, focus_x, desc_y);
		cairo_text_path(cr, "Focus");
		cairo_stroke(cr);
	}

	// Draw the fill for the headings
	cairo_set_source_rgba(cr, 1, 1, 1, 1);
	cairo_move_to(cr, gain_x, desc_y);
	cairo_show_text(cr, "Gain");
	cairo_move_to(cr, exposure_x, desc_y);
	cairo_show_text(cr, "Exposure");
	cairo_move_to(cr, balance_x, desc_y);
	cairo_show_text(cr, "Balance");
	if (camera->hasfocus) {
		cairo_move_to(cr, focus_x, desc_y);
		cairo_show_text(cr, "Focus");
	}

	// Draw the outlines for the values
	cairo_select_font_face(cr, "sans-serif", CAIRO_FONT_SLANT_NORMAL,
			       CAIRO_FONT_WEIGHT_NORMAL);
	cairo_set_font_size(cr, 11);
	cairo_set_source_rgba(cr, 0, 0, 0, 1);

	cairo_move_to(cr, gain_x, value_y);
	cairo_text_path(cr, iso);
	cairo_stroke(cr);

	cairo_move_to(cr, exposure_x, value_y);
	cairo_text_path(cr, shutterangle);
	cairo_stroke(cr);

	cairo_move_to(cr, balance_x, value_y);
	cairo_text_path(cr, wb_text);
	cairo_stroke(cr);

	if (camera->hasfocus) {
		cairo_move_to(cr, focus_x, value_y);
		cairo_text_path(cr, f);
		cairo_stroke(cr);
	}

	// Draw the fill for the values
	cairo_set_source_rgba(cr, 1, 1, 1, 1);
	cairo_move_to(cr, gain_x, value_y);
	cairo_show_text(cr, iso);
	cairo_move_to(cr, exposure_x, value_y);
	cairo_show_text(cr, shutterangle);
	cairo_move_to(cr, balance_x, value_y);
	cairo_show_text(cr, wb_text);
	if (camera->hasfocus) {
		cairo_move_to(cr, focus_x, value_y);
		cairo_show_text(cr, f);
	}

	cairo_move_to(cr, debug_x, value_y);
	cairo_text_path(cr, debug_message);
	cairo_stroke(cr);

	cairo_destroy(cr);

	gtk_widget_queue_draw_area(preview, 0, 0, preview_width, 32);
}

static gboolean
preview_draw(GtkWidget *widget, cairo_t *cr, gpointer data)
{
	if (!camera_is_initialized) {
		return FALSE;
	}

	// Clear preview area with black
	cairo_paint(cr);

	if (surface) {
		// Draw camera preview
		cairo_save(cr);

		int width = cairo_image_surface_get_width(surface);
		int height = cairo_image_surface_get_height(surface);
		transform_centered(cr, preview_width, preview_height, width, height);

		cairo_set_source_surface(cr, surface, 0, 0);
		cairo_paint(cr);

		// Draw zbar image
		if (zbar_result) {
			for (uint8_t i = 0; i < zbar_result->size; ++i) {
				MPZBarCode *code = &zbar_result->codes[i];

				cairo_set_line_width(cr, 3.0);
				cairo_set_source_rgba(cr, 0, 0.5, 1, 0.75);
				cairo_new_path(cr);
				cairo_move_to(cr, code->bounds_x[0], code->bounds_y[0]);
				for (uint8_t i = 0; i < 4; ++i) {
					cairo_line_to(cr, code->bounds_x[i], code->bounds_y[i]);
				}
				cairo_close_path(cr);
				cairo_stroke(cr);

				cairo_save(cr);
				cairo_translate(cr, code->bounds_x[0], code->bounds_y[0]);
				cairo_show_text(cr, code->data);
				cairo_restore(cr);
			}
		}

		cairo_restore(cr);
	}

	// Draw control overlay
	cairo_set_source_surface(cr, status_surface, 0, 0);
	cairo_paint(cr);
	return FALSE;
}

static gboolean
preview_configure(GtkWidget *widget, GdkEventConfigure *event)
{
	int new_preview_width = gtk_widget_get_allocated_width(widget);
	int new_preview_height = gtk_widget_get_allocated_height(widget);

	if (preview_width != new_preview_width ||
	    preview_height != new_preview_height) {
		preview_width = new_preview_width;
		preview_height = new_preview_height;
		update_io_pipeline();
	}

	draw_controls();

	return TRUE;
}

void
on_open_last_clicked(GtkWidget *widget, gpointer user_data)
{
	char uri[275];
	GError *error = NULL;

	if (strlen(last_path) == 0) {
		return;
	}
	sprintf(uri, "file://%s", last_path);
	if (!g_app_info_launch_default_for_uri(uri, NULL, &error)) {
		g_printerr("Could not launch image viewer for '%s': %s\n", uri, error->message);
	}
}

void
notify_movie_progress(void)
{
	if (!movie_start)
		gtk_button_set_label(GTK_BUTTON(movie), "Rec");
}

void
on_movie_clicked(GtkWidget *widget, gpointer user_data)
{
	if (movie_start) {
		movie_start = 0;
		gtk_button_set_label(GTK_BUTTON(movie), "Busy");
		on_movie_stop();
	} else {
		movie_start = 1;
		gtk_button_set_label(GTK_BUTTON(movie), "Stop");
		on_movie_start();
	}
}

void
on_open_directory_clicked(GtkWidget *widget, gpointer user_data)
{
	char uri[270];
	GError *error = NULL;

	sprintf(uri, "file://%s", g_get_user_special_dir(G_USER_DIRECTORY_PICTURES));
	if (!g_app_info_launch_default_for_uri(uri, NULL, &error)) {
		g_printerr("Could not launch image viewer: %s\n", error->message);
	}
}

void
on_shutter_clicked(GtkWidget *widget, gpointer user_data)
{
	gtk_spinner_start(GTK_SPINNER(process_spinner));
	gtk_stack_set_visible_child(GTK_STACK(open_last_stack), process_spinner);
	mp_io_pipeline_capture();
}

void
on_capture_shortcut(void)
{
	on_shutter_clicked(NULL, NULL);
}

static bool
check_point_inside_bounds(int x, int y, int *bounds_x, int *bounds_y)
{
	bool right = false, left = false, top = false, bottom = false;

	for (int i = 0; i < 4; ++i) {
		if (x <= bounds_x[i])
			left = true;
		if (x >= bounds_x[i])
			right = true;
		if (y <= bounds_y[i])
			top = true;
		if (y >= bounds_y[i])
			bottom = true;
	}

	return right && left && top && bottom;
}

static void
on_zbar_code_tapped(GtkWidget *widget, const MPZBarCode *code)
{
	GtkWidget *dialog;
	GtkDialogFlags flags = GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT;
        bool data_is_url =
                g_uri_is_valid(code->data, G_URI_FLAGS_PARSE_RELAXED, NULL);

	char* data = strdup(code->data);

	if (data_is_url) {
		dialog = gtk_message_dialog_new(
			GTK_WINDOW(gtk_widget_get_toplevel(widget)),
			flags,
			GTK_MESSAGE_QUESTION,
			GTK_BUTTONS_NONE,
			"Found a URL '%s' encoded in a %s code.",
			code->data,
			code->type);
		gtk_dialog_add_buttons(
			GTK_DIALOG(dialog),
			"_Open URL",
			GTK_RESPONSE_YES,
			NULL);
	} else {
		dialog = gtk_message_dialog_new(
			GTK_WINDOW(gtk_widget_get_toplevel(widget)),
			flags,
			GTK_MESSAGE_QUESTION,
			GTK_BUTTONS_NONE,
			"Found '%s' encoded in a %s code.",
			code->data,
			code->type);
	}
	gtk_dialog_add_buttons(
		GTK_DIALOG(dialog),
		"_Copy",
		GTK_RESPONSE_ACCEPT,
		"_Cancel",
		GTK_RESPONSE_CANCEL,
		NULL);

	int result = gtk_dialog_run(GTK_DIALOG(dialog));

	GError *error = NULL;
	switch (result) {
		case GTK_RESPONSE_YES:
			if (!g_app_info_launch_default_for_uri(data,
							       NULL, &error)) {
				g_printerr("Could not launch browser: %s\n",
					   error->message);
			}
		case GTK_RESPONSE_ACCEPT:
			gtk_clipboard_set_text(
				gtk_clipboard_get(GDK_SELECTION_CLIPBOARD),
				data, -1);
		case GTK_RESPONSE_CANCEL:
			break;
		default:
			g_printerr("Wrong dialog result: %d\n", result);
	}
	gtk_widget_destroy(dialog);
}

void
on_preview_tap(GtkWidget *widget, GdkEventButton *event, gpointer user_data)
{
	if (event->type != GDK_BUTTON_PRESS)
		return;

	// Handle taps on the controls
	if (event->y < max_y) {
		if (event->x < exposure_x) {
			if (current_control == USER_CONTROL_ISO && gtk_widget_is_visible(control_box)) {
				gtk_widget_hide(control_box);
				return;
			}
			// ISO
			current_control = USER_CONTROL_ISO;
			gtk_label_set_text(GTK_LABEL(control_name), "Gain");
			gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(control_auto),
						     !gain_is_manual);
			gtk_adjustment_set_lower(control_slider, (float)gain_min);
			gtk_adjustment_set_upper(control_slider, (float)gain_max);
			gtk_adjustment_set_value(control_slider, (double)gain);
		} else if (event->x > exposure_x && event->x < balance_x) {
			if (current_control == USER_CONTROL_SHUTTER && gtk_widget_is_visible(control_box)) {
				gtk_widget_hide(control_box);
				return;
			}
			// Shutter angle
			current_control = USER_CONTROL_SHUTTER;
			gtk_label_set_text(GTK_LABEL(control_name), "Exposure");
			gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(control_auto),
						     !exposure_is_manual);
			gtk_adjustment_set_lower(control_slider, (float)exposure_min);
			gtk_adjustment_set_upper(control_slider, (float)exposure_max);
			gtk_adjustment_set_value(control_slider, (double)exposure);
		} else if (event->x > balance_x && event->x < focus_x) {
			if (current_control == USER_CONTROL_WHITE_BALANCE && gtk_widget_is_visible(control_box)) {
				gtk_widget_hide(control_box);
				return;
			}
			// White balance
			current_control = USER_CONTROL_WHITE_BALANCE;
			gtk_label_set_text(GTK_LABEL(control_name), "Balance");
			gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(control_auto),
						     false);
			gtk_adjustment_set_lower(control_slider, 0.0);
			gtk_adjustment_set_upper(control_slider, sizeof(WB_ILLUMINANTS) / sizeof(WB_ILLUMINANTS[0]) - 1);
			gtk_adjustment_set_value(control_slider, wb);
		} else if (event->x > focus_x && event->x < debug_x && camera->hasfocus) {
			if (current_control == USER_CONTROL_FOCUS && gtk_widget_is_visible(control_box)) {
				gtk_widget_hide(control_box);
				return;
			}
			current_control = USER_CONTROL_FOCUS;
			gtk_label_set_text(GTK_LABEL(control_name), "Focus");
			gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(control_auto),
						     false);
			gtk_adjustment_set_lower(control_slider, 0.0);
			gtk_adjustment_set_upper(control_slider, DW9714_FOCUS_VAL_MAX);
			gtk_adjustment_set_value(control_slider, (1.0 - sqrt(focus / (double)DW9714_FOCUS_VAL_MAX)) * DW9714_FOCUS_VAL_MAX);
		}
		gtk_widget_show(control_box);

		return;
	}

	// Tapped zbar result
	if (zbar_result) {
		// Transform the event coordinates to the image
		int width = cairo_image_surface_get_width(surface);
		int height = cairo_image_surface_get_height(surface);
		double scale = MIN(preview_width / (double)width, preview_height / (double)height);
		int x = (event->x - preview_width / 2) / scale + width / 2;
		int y = (event->y - preview_height / 2) / scale + height / 2;

		for (uint8_t i = 0; i < zbar_result->size; ++i) {
			MPZBarCode *code = &zbar_result->codes[i];

			if (check_point_inside_bounds(x, y, code->bounds_x, code->bounds_y)) {
				on_zbar_code_tapped(widget, code);
				return;
			}
		}
	}

	// Tapped preview image itself, try focussing
	if (has_auto_focus_start) {
		mp_io_pipeline_focus();
	}
}

void
on_error_close_clicked(GtkWidget *widget, gpointer user_data)
{
	gtk_widget_hide(error_box);
}

void
on_camera_switch_clicked(GtkWidget *widget, gpointer user_data)
{
	size_t next_index = camera->index + 1;
	const struct mp_camera_config *next_camera =
		mp_get_camera_config(next_index);

	if (!next_camera) {
		next_index = 0;
		next_camera = mp_get_camera_config(next_index);
	}

	camera = next_camera;
	update_io_pipeline();
	draw_controls();
}

void
on_settings_btn_clicked(GtkWidget *widget, gpointer user_data)
{
	gtk_stack_set_visible_child_name(GTK_STACK(main_stack), "settings");
}

void
on_back_clicked(GtkWidget *widget, gpointer user_data)
{
	gtk_stack_set_visible_child_name(GTK_STACK(main_stack), "main");
}

void
on_control_auto_toggled(GtkToggleButton *widget, gpointer user_data)
{
	bool is_manual = gtk_toggle_button_get_active(widget) ? false : true;
	bool has_changed;

	switch (current_control) {
	case USER_CONTROL_ISO:
		if (gain_is_manual != is_manual) {
			gain_is_manual = is_manual;
			has_changed = true;
		}
		break;
	case USER_CONTROL_SHUTTER:
		if (exposure_is_manual != is_manual) {
			exposure_is_manual = is_manual;
			has_changed = true;
		}
		break;
	case USER_CONTROL_FOCUS:
		focus_is_manual = is_manual;
		if (is_manual)
			has_changed = true;
		else
			auto_focus_start();
		break;
	case USER_CONTROL_WHITE_BALANCE:
		wb_is_manual = is_manual;
		if (is_manual)
			has_changed = true;
		break;
	}

	if (has_changed && sw_auto) {
		// When doing gain/exposure in software, we can sync the slider with
		// actual state, which is better than option below.
		double value;

		switch (current_control) {
		case USER_CONTROL_ISO:
			value = (double) gain;
			break;
		case USER_CONTROL_SHUTTER:
			value = (double) exposure;
			break;
		case USER_CONTROL_FOCUS:
			value = (double) (1.0 - sqrt(focus / (double)DW9714_FOCUS_VAL_MAX)) * DW9714_FOCUS_VAL_MAX;
			break;
		case USER_CONTROL_WHITE_BALANCE:
			value = (double) wb;
			break;
		default:
			printf("Unexpected control\n");
			return;
		}

		gtk_adjustment_set_value(control_slider, (double)value);
		draw_controls();
	}

	if (has_changed && !sw_auto) {
		// When we have hardware controlling gain/exposure, the best we can do is
		// to sync hardware with sliders.
		// The slider might have been moved while Auto mode is active. When entering
		// Manual mode, first read the slider value to sync with those changes.
		double value = gtk_adjustment_get_value(control_slider);
		switch (current_control) {
		case USER_CONTROL_ISO:
			if (value != gain) {
				gain = (int)value;
			}
			break;
		case USER_CONTROL_SHUTTER: {
			// So far all sensors use exposure time in number of sensor rows
			int new_exposure = value;
			if (new_exposure != exposure) {
				exposure = new_exposure;
			}
			break;
		}
		case USER_CONTROL_FOCUS:
			focus = value;
			break;
		case USER_CONTROL_WHITE_BALANCE:
			wb = value;
			break;
		}

		update_io_pipeline();
		draw_controls();
	}
}

void
on_control_slider_changed(GtkAdjustment *widget, gpointer user_data)
{
	double value = gtk_adjustment_get_value(widget);

	bool has_changed = false;
	switch (current_control) {
	case USER_CONTROL_ISO:
		if (value != gain) {
			gain = (int)value;
			has_changed = true;
		}
		break;
	case USER_CONTROL_SHUTTER: {
		// So far all sensors use exposure time in number of sensor rows
		int new_exposure = value;
		if (new_exposure != exposure) {
			exposure = new_exposure;
			has_changed = true;
		}
		break;
	}
	case USER_CONTROL_WHITE_BALANCE: {
		if (value != wb) {
			wb = value;
			has_changed = true;
			mp_calculate_matrices(wb);
		}
		break;
	}
	case USER_CONTROL_FOCUS: {
		int new = round(DW9714_FOCUS_VAL_MAX * pow(1.0 - value / (double)DW9714_FOCUS_VAL_MAX, 2));
		if (new != focus) {
			focus = new;
			has_changed = true;
			set_focus(focus);
		}
		break;
	}
	}

	if (has_changed) {
		update_io_pipeline();
		draw_controls();
	}
}

void on_quit(void) {
	mp_io_pipeline_release();
	gtk_main_quit();
}

bool check_window_active(void)
{
	return gtk_window_is_active(GTK_WINDOW(window));
}

int
main(int argc, char *argv[])
{
	if (!mp_load_config())
		return 1;

	mp_calculate_matrices(wb);

	setenv("LC_NUMERIC", "C", 1);

	gtk_init(&argc, &argv);
	g_set_prgname("sm.puri.Millipixels");
	g_object_set(gtk_settings_get_default(), "gtk-application-prefer-dark-theme",
		     TRUE, NULL);
	GtkBuilder *builder = gtk_builder_new_from_resource(
		"/org/postmarketos/Megapixels/camera.glade");

	window = GTK_WIDGET(gtk_builder_get_object(builder, "window"));
	GtkWidget *shutter = GTK_WIDGET(gtk_builder_get_object(builder, "shutter"));
	GtkWidget *switch_btn =
		GTK_WIDGET(gtk_builder_get_object(builder, "switch_camera"));
	GtkWidget *settings_btn =
		GTK_WIDGET(gtk_builder_get_object(builder, "settings"));
	GtkWidget *settings_back =
		GTK_WIDGET(gtk_builder_get_object(builder, "settings_back"));
	GtkWidget *error_close =
		GTK_WIDGET(gtk_builder_get_object(builder, "error_close"));
	GtkWidget *open_last =
		GTK_WIDGET(gtk_builder_get_object(builder, "open_last"));
	GtkWidget *open_directory =
		GTK_WIDGET(gtk_builder_get_object(builder, "open_directory"));
	movie = GTK_WIDGET(gtk_builder_get_object(builder, "movie"));

	preview = GTK_WIDGET(gtk_builder_get_object(builder, "preview"));
	error_box = GTK_WIDGET(gtk_builder_get_object(builder, "error_box"));
	error_message = GTK_WIDGET(gtk_builder_get_object(builder, "error_message"));
	main_stack = GTK_WIDGET(gtk_builder_get_object(builder, "main_stack"));
	open_last_stack = GTK_WIDGET(gtk_builder_get_object(builder, "open_last_stack"));
	thumb_last = GTK_WIDGET(gtk_builder_get_object(builder, "thumb_last"));
	process_spinner = GTK_WIDGET(gtk_builder_get_object(builder, "process_spinner"));
	control_box = GTK_WIDGET(gtk_builder_get_object(builder, "control_box"));
	control_name = GTK_WIDGET(gtk_builder_get_object(builder, "control_name"));
	control_slider =
		GTK_ADJUSTMENT(gtk_builder_get_object(builder, "control_adj"));
	control_auto = GTK_WIDGET(gtk_builder_get_object(builder, "control_auto"));
	camera_missing = GTK_WIDGET(gtk_builder_get_object(builder, "camera_missing"));
	g_signal_connect(window, "destroy", G_CALLBACK(on_quit), NULL);
	g_signal_connect(shutter, "clicked", G_CALLBACK(on_shutter_clicked), NULL);
	g_signal_connect(error_close, "clicked", G_CALLBACK(on_error_close_clicked),
			 NULL);
	g_signal_connect(switch_btn, "clicked", G_CALLBACK(on_camera_switch_clicked),
			 NULL);
	g_signal_connect(settings_btn, "clicked",
			 G_CALLBACK(on_settings_btn_clicked), NULL);
	g_signal_connect(settings_back, "clicked", G_CALLBACK(on_back_clicked),
			 NULL);
	g_signal_connect(open_last, "clicked", G_CALLBACK(on_open_last_clicked),
			 NULL);
	g_signal_connect(open_directory, "clicked",
			 G_CALLBACK(on_open_directory_clicked), NULL);
	g_signal_connect(movie, "clicked",
			 G_CALLBACK(on_movie_clicked), NULL);
	g_signal_connect(preview, "draw", G_CALLBACK(preview_draw), NULL);
	g_signal_connect(preview, "configure-event", G_CALLBACK(preview_configure),
			 NULL);
	gtk_widget_set_events(preview, gtk_widget_get_events(preview) |
					       GDK_BUTTON_PRESS_MASK |
					       GDK_POINTER_MOTION_MASK);
	g_signal_connect(preview, "button-press-event", G_CALLBACK(on_preview_tap),
			 NULL);
	g_signal_connect(control_auto, "toggled",
			 G_CALLBACK(on_control_auto_toggled), NULL);
	g_signal_connect(control_slider, "value-changed",
			 G_CALLBACK(on_control_slider_changed), NULL);

	GtkCssProvider *provider = gtk_css_provider_new();
	if (access("camera.css", F_OK) != -1) {
		gtk_css_provider_load_from_path(provider, "camera.css", NULL);
	} else {
		gtk_css_provider_load_from_resource(
			provider, "/org/postmarketos/Megapixels/camera.css");
	}
	GtkStyleContext *context = gtk_widget_get_style_context(error_box);
	gtk_style_context_add_provider(context, GTK_STYLE_PROVIDER(provider),
				       GTK_STYLE_PROVIDER_PRIORITY_USER);
	context = gtk_widget_get_style_context(control_box);
	gtk_style_context_add_provider(context, GTK_STYLE_PROVIDER(provider),
				       GTK_STYLE_PROVIDER_PRIORITY_USER);

	GClosure* capture_shortcut = g_cclosure_new(on_capture_shortcut, 0, 0);

	GtkAccelGroup* accel_group = gtk_accel_group_new();
	gtk_accel_group_connect(accel_group,
			GDK_KEY_space,
			0,
			0,
			capture_shortcut);

	gtk_window_add_accel_group(GTK_WINDOW(window), accel_group);

	mp_io_pipeline_start();

	camera = mp_get_camera_config(0);
	update_io_pipeline();

	gtk_widget_show(window);
	gtk_main();

	mp_io_pipeline_stop();

	return 0;
}
