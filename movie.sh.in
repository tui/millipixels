#!/bin/bash
# Copyright 2022 Pavel Machek, GPLv2+

jpegize() {
	DNG_DIR="$1"
	BURST_DIR="$GIGA_DIR/sm/"

	DCRAW=dcraw
	TIFF_EXT="tiff"
	set --

	CONVERT="convert"

	cd $DNG_DIR
	I=0
	NUM=0
	for DNG in *.dng; do
		NUM=$[$NUM+1]
	done

	for DNG in *.dng; do
		PERC=$[(100*$I)/$NUM]
		echo $PERC
		BASE=${DNG%%.dng}
		# -w		Use camera white balance
		# +M		Use embedded color matrix
		# -H 2		Recover highlights by blending them
		# -o 1		Output in sRGB colorspace
		# -q 0		Debayer with fast bi-linear interpolation
		# -f		Interpolate RGGB as four colors
		# -T		Output TIFF
		(
			$DCRAW -w +M -H 2 -o 1 -q 0 -f -T "$DNG"
			$CONVERT "$BASE.tiff" "$BASE.jpeg"
			rm "$BASE.tiff"
			mv "$BASE.jpeg" "$BURST_DIR/$BASE.jpeg.sv"
		) &
		I=$[$I+1]
		if [ 0 == $[ $I % 16 ] ]; then
			wait
		fi
	done
}

SCRIPT_DIR=$(cd $(dirname "${BASH_SOURCE[0]}") && pwd)
GIGA_DIR="$2"
DEST_NAME="$3"

if [ "-$1" == "-start" ]; then
	mkdir $GIGA_DIR/sm
	cd $GIGA_DIR/sm
	@LIBEXECDIR@/movie_audio_rec &
	echo $! > $2/audio.pid
elif [ "-$1" == "-stop" ]; then
	kill `cat $2/audio.pid`
	jpegize $2 # | zenity --progress "--text=Converting, phase 1, dng -> jpeg" --time-remaining
	cd $GIGA_DIR/sm
	$SCRIPT_DIR/mpegize.py convertall $GIGA_DIR/
	mv $GIGA_DIR/smo/*.mp4 $DEST_NAME
	rm -r $GIGA_DIR
else
	echo "Unrecognized command"
fi
