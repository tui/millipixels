#pragma once

#define NUM_WB 7

extern char* WB_ILLUMINANTS[NUM_WB];
extern float WB_WHITEPOINTS[NUM_WB][3];
